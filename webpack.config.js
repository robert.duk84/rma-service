let Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');

Encore

    .autoProvidejQuery()
    .setOutputPath('public/build/')
    .setPublicPath('/build')

    //JS
    .addEntry('app', './assets/js/app.js')
    .addEntry('panel-layout', './assets/js/panel-layout.js')
    .addEntry('home', './assets/js/home.js')

    //CSS
    .addStyleEntry('css/panel', './assets/css/app.css')
    .addStyleEntry('css/home', './assets/css/home.css')

    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(false)
    .enableSassLoader()

    // uncomment if you use API Platform Admin (composer req api-admin)
    //.enableReactPreset()
    //.addEntry('admin', './assets/js/admin.js')
    .addPlugin(new CopyWebpackPlugin([
        {
            from: './assets/img', to: 'images',
        },
    ]))
;

module.exports = Encore.getWebpackConfig();
