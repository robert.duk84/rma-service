<?php

namespace App\Form;

use App\Entity\RmaNotification;
use App\Entity\RmaStatus;
use App\Entity\WarrantyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RmaNotificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('serialNumber', TextType::class,
                [
                    'label' => 'Numer seryjny*: ',
                    'required' => true,
                    'attr' => [
                        'maxlength' => 25
                    ]
                ])
            ->add('productCode', TextType::class,
                [
                    'label'=> 'Kod produktu*: ',
                    'required' => true,
                    'attr' => [
                        'maxlength' => 25
                    ]
                ])
            ->add('productName', TextType::class,
                [
                    'label'=> 'Nazwa produktu*: ',
                    'required' => true,
                    'attr' => [
                        'maxlength' => 50
                    ]
                ])
            ->add('purchaseDate', DateType::class,
                [
                    'label' => 'Data zakupu*: ',
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd'
                ])
            ->add('purchaseDocument', TextType::class,
                [
                    'label'=> 'Dokument zakupu*: ',
                    'required' => true,
                    'attr' => [
                        'maxlength' => 25,
                        'placeholder' => 'Nr paragonu/faktury'
                    ]
                ])
            ->add('warrantyCard', TextType::class,
                [
                    'label'=> 'Numer karty gwarancyjnej: ',
                    'required' => false,
                    'attr' => [
                        'maxlength' => 25,
                        'placeholder' => 'Jeżeli jest posiadany',
                    ]
                ])
            ->add('purchasePrice', TextType::class,
                [
                    'label'=> 'Cena zakupu: ',
                    'required' => false,
                    'attr' => [
                        'maxlength' => 10,
                        'placeholder' => 'Nie wymagane',
                    ]
                ])
            ->add('externalComplainCode', TextType::class,
                [
                    'label'=> 'Zwenętrzny numer RMA: ',
                    'required' => false,
                    'attr' => [
                        'maxlength' => 25,
                        'placeholder' => 'Nie wymagane',
                    ]
                ])
            ->add('damageDescription', TextareaType::class,
                [
                    'label'=> 'Opis usterki/wady*: ',
                    'required' => true,
                    'attr' => [
                        'placeholder' => 'Wymagane',
                        'rows' => 4
                    ]
                ])
            ->add('configurationSpecification', TextareaType::class,
                [
                    'label'=> 'Konfiguracja użytkownika/specyfikacja ustawień: ',
                    'required' => false,
                    'attr' => [
                        'placeholder' => 'Nie wymagane',
                        'rows' => 4
                    ]
                ])
            ->add('warrantyType', EntityType::class,
                [
                    'label'=> 'Sposób realizacji reklamacji*: ',
                    'class' => WarrantyType::class,
                    'required' => true,
                    'placeholder' => 'Wybierz sposób realizacji reklamacji',
                ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RmaNotification::class,
        ]);
    }
}
