<?php

namespace App\Form;

use App\Entity\UserProfile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserProfileForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $option)
    {
        $builder->add('name', null, [
            'label' => 'Imię',
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Imię'
            ]
        ])
            ->add('surname', null, [
                'label' => 'Nazwisko',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nazwisko'
                ]
            ])
            ->add('companyName', null, [
                'label' => 'Nazwa firmy',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nazwa firmy'
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserProfile::class
        ]);
    }

}
