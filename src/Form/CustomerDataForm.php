<?php

namespace App\Form;

use App\Entity\CustomerCategory;
use App\Entity\CustomerData;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

class CustomerDataForm extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('tax_number', null, [
            'label' => ('NIP'),
            'attr' => [
                'class' => 'customer_tax_number',
                'placeholder' => ('NIP')
            ]
        ])
            ->add('companyName', null, [
                'label' => ('Nazwa firmy'),
                'attr' => [
                    'class' => 'customer_name',
                    'maxlength' => 90,
                    'placeholder' => ('Nazwa firmy (dane do faktury)')
                ]
            ])
            ->add('customerCategory', EntityType::class, [
                'label' => ('Typ biznesu'),
                'class' => CustomerCategory::class,
                'required' => true,
                'placeholder' => 'Wybierz typ biznesu',
            ])
            ->add('streetName', null, [
                'label' => ('Ulica'),
                'attr' => [
                    'maxlength' => 90,
                    'class' => 'customer_streetName',
                    'placeholder' => ('Ulica')
                ]
            ])
            ->add('houseNumber', null, [
                'label' => ('Numer domu'),
                'attr' => [
                    'maxlength' => 6,
                    'class' => 'customer_houseNumber',
                    'placeholder' => ('Numer domu')
                ]
            ])
            ->add('apartmentNumber', null, [
                'label' => ('Numer lokalu'),
                'attr' => [
                    'maxlength' => 6,
                    'class' => 'customer_apartmentNumber',
                    'placeholder' => ('Numer lokalu')
                ]
            ])
            ->add('postCode', null, [
                'label' => ('Kod pocztowy'),
                'attr' => [
                    'maxlength' => 6,
                    'class' => 'customer_postCode',
                    'placeholder' => ('Kod pocztowy')
                ]
            ])
            ->add('city', null, [
                'label' => ('Miasto'),
                'attr' => [
                    'maxlength' => 60,
                    'class' => 'customer_city',
                    'placeholder' => ('Miasto')
                ]
            ])
            ->add('alternativeEmail', EmailType::class, [
                'label' => ('E-mail kontaktowy'),
                'attr' => [
                    'maxlength' => 90,
                ]
            ])
            ->add('mobileNumber', null, [
                'label' => ('Telefon komórkowy'),
                'attr' => [
                    'maxlength' => 10,
                    'placeholder' => ('Telefon')
                ]
            ])
            ->add('additionalInfo', TextareaType::class, [
                'required' => false,
                'label' => ('Informacje dodatkowe'),
                'attr' => [
                    'placeholder' => ('Informacje dodatkowe'),
                    'rows' => 4
                ]
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'label' => ('Wyrażam zgodę na przetwarzanie moich danych osobowych dla potrzeb niezbędnych do 
                           realizacji procesu reklamacji (zgodnie z ustawą z dnia 10 maja 2018 roku o ochronie 
                           danych osobowych (Dz. Ustaw z 2018, poz. 1000) oraz zgodnie z Rozporządzeniem Parlamentu 
                           Europejskiego i Rady (UE) 2016/679 z dnia 27 kwietnia 2016 r. w sprawie ochrony osób 
                           fizycznych w związku z przetwarzaniem danych osobowych i w sprawie swobodnego 
                           przepływu takich danych oraz uchylenia dyrektywy 95/46/WE (RODO)). (*). '),
                'constraints' => [
                    new IsTrue([
                        'message' => 'Proszę o zaznaczenie zgody w celu rejestracji.'
                        ])
                    ]]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CustomerData::class
        ]);
    }
}
