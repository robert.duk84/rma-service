<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190611224929 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Rma Notification Status';
    }

    public function up(Schema $schema) : void
    {
        foreach ($this->getRmaStatuses() as $status)
        {
            $this->addSql("INSERT INTO rma_status (name) VALUES (?)", [$status['name']]);
        }
    }

    private function getRmaStatuses(): array
    {
        return [
            ['name' => 'wprowadzono zgłoszenie'],
            ['name' => 'otrzymanie produktu'],
            ['name' => 'przyjecie RMA do realizacji'],
            ['name' => 'identyfikacja usterki'],
            ['name' => 'oczekiwanie na podzespoły'],
            ['name' => 'testowanie urządzenia'],
            ['name' => 'wysyłka do klienta'],
            ['name' => 'zakończona'],
        ];
    }

    public function down(Schema $schema) : void
    {
        foreach ($this->getRmaStatuses() as $status)
        {
            $this->addSql("DELETE FROM rma_status WHERE name = ?", [$status['name']]);
        }
    }
}
