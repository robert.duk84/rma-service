<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190522093601 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Warranty type list';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        foreach ($this->getWarrantyTypes() as $status)
        {
            $this->addSql("INSERT INTO warranty_type (name) VALUES (?)", [$status['name']]);
        }
    }

    private function getWarrantyTypes(): array
    {
        return [
            ['name' => 'gwarancja'],
            ['name' => 'rękojmia'],
            ['name' => 'kontrakt'],
            ['name' => 'odpłatna'],
            ['name' => 'inna'],
        ];
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        foreach ($this->getWarrantyTypes() as $status)
        {
            $this->addSql("DELETE FROM warranty_type WHERE name = ?", [$status['name']]);
        }
    }
}

