<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190522093421 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE rma_notification (id INT AUTO_INCREMENT NOT NULL, warranty_type_id INT NOT NULL, user_id INT DEFAULT NULL, created_at DATETIME NOT NULL, serial_number VARCHAR(25) NOT NULL, product_code VARCHAR(25) NOT NULL, product_name VARCHAR(50) NOT NULL, purchase_date DATETIME NOT NULL, purchase_document VARCHAR(25) NOT NULL, warranty_card VARCHAR(25) DEFAULT NULL, purchase_price VARCHAR(10) DEFAULT NULL, external_complain_code VARCHAR(25) DEFAULT NULL, damage_description LONGTEXT DEFAULT NULL, configuration_specification LONGTEXT DEFAULT NULL, updated_at DATETIME NOT NULL, INDEX IDX_F1E4D67E22995FA7 (warranty_type_id), INDEX IDX_F1E4D67EA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE warranty_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rma_notification ADD CONSTRAINT FK_F1E4D67E22995FA7 FOREIGN KEY (warranty_type_id) REFERENCES warranty_type (id)');
        $this->addSql('ALTER TABLE rma_notification ADD CONSTRAINT FK_F1E4D67EA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rma_notification DROP FOREIGN KEY FK_F1E4D67E22995FA7');
        $this->addSql('DROP TABLE rma_notification');
        $this->addSql('DROP TABLE warranty_type');
    }
}
