<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190519015755 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE customer_category (id INT AUTO_INCREMENT NOT NULL, category_name VARCHAR(30) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer_data ADD customer_category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE customer_data ADD CONSTRAINT FK_F9625714110DB6EA FOREIGN KEY (customer_category_id) REFERENCES customer_category (id)');
        $this->addSql('CREATE INDEX IDX_F9625714110DB6EA ON customer_data (customer_category_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE customer_data DROP FOREIGN KEY FK_F9625714110DB6EA');
        $this->addSql('DROP TABLE customer_category');
        $this->addSql('DROP INDEX IDX_F9625714110DB6EA ON customer_data');
        $this->addSql('ALTER TABLE customer_data DROP customer_category_id');
    }
}
