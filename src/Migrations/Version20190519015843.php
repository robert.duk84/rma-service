<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190519015843 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Customer business category list';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        foreach ($this->getCustomerCategories() as $status)
        {
            $this->addSql("INSERT INTO customer_category (category_name) VALUES (?)", [$status['category_name']]);
        }
    }

    private function getCustomerCategories(): array
    {
        return [
            ['category_name' => 'Sklep'],
            ['category_name' => 'Serwis'],
            ['category_name' => 'Firma prywatna'],
            ['category_name' => 'Przedstawiciel handlowy'],
            ['category_name' => 'Hurtownia'],
            ['category_name' => 'Dystrybutor'],
        ];
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        foreach ($this->getCustomerCategories() as $status)
        {
            $this->addSql("DELETE FROM customer_category WHERE category_name = ?", [$status['category_name']]);
        }
    }
}
