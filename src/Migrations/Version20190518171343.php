<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190518171343 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE customer_data (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, tax_number VARCHAR(10) NOT NULL, company_name VARCHAR(255) NOT NULL, street_name VARCHAR(255) NOT NULL, house_number VARCHAR(20) NOT NULL, apartment_number VARCHAR(20) DEFAULT NULL, post_code VARCHAR(6) NOT NULL, city VARCHAR(255) DEFAULT NULL, alternative_email VARCHAR(255) DEFAULT NULL, mobile_number VARCHAR(12) NOT NULL, additional_info VARCHAR(450) DEFAULT NULL, UNIQUE INDEX UNIQ_F9625714A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer_data ADD CONSTRAINT FK_F9625714A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE customer_data');
    }
}
