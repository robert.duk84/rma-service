<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190612022540 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rma_notification ADD rma_status_id INT DEFAULT NULL, DROP rma_status');
        $this->addSql('ALTER TABLE rma_notification ADD CONSTRAINT FK_F1E4D67E379A6285 FOREIGN KEY (rma_status_id) REFERENCES rma_status (id)');
        $this->addSql('CREATE INDEX IDX_F1E4D67E379A6285 ON rma_notification (rma_status_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rma_notification DROP FOREIGN KEY FK_F1E4D67E379A6285');
        $this->addSql('DROP INDEX IDX_F1E4D67E379A6285 ON rma_notification');
        $this->addSql('ALTER TABLE rma_notification ADD rma_status VARCHAR(255) DEFAULT \'0\' COLLATE utf8mb4_unicode_ci, DROP rma_status_id');
    }
}
