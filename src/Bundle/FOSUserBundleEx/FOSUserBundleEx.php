<?php

namespace App\Bundle\FOSUserBundleEx;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class FOSUserBundleEx extends Bundle
{

    public function getParent()
    {
        return 'FOSUserBundle';
    }

}
