<?php

namespace App\Bundle\FOSUserBundleEx\Form\Type;

use App\Form\UserProfileForm;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseRegistrationFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('userProfile', UserProfileForm::class);
        $builder->remove('username');
    }

    /**
     * @return string
     */
    public function getParent()
    {
        return BaseRegistrationFormType::class;
    }

}
  
