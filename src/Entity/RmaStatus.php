<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RmaStatusRepository")
 */
class RmaStatus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RmaNotification", mappedBy="rmaStatus")
     */
    private $rmaNotification;

    public function __construct()
    {
        $this->rmaNotification = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|RmaNotification[]
     */
    public function getRmaNotification(): Collection
    {
        return $this->rmaNotification;
    }

    public function addRmaNotification(RmaNotification $rmaNotification): self
    {
        if (!$this->rmaNotification->contains($rmaNotification)) {
            $this->rmaNotification[] = $rmaNotification;
            $rmaNotification->setRmaStatus($this);
        }

        return $this;
    }

    public function removeRmaNotification(RmaNotification $rmaNotification): self
    {
        if ($this->rmaNotification->contains($rmaNotification)) {
            $this->rmaNotification->removeElement($rmaNotification);
            // set the owning side to null (unless already changed)
            if ($rmaNotification->getRmaStatus() === $this) {
                $rmaNotification->setRmaStatus(null);
            }
        }

        return $this;
    }
}
