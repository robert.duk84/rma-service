<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RmaNotificationRepository")
 * @ORM\Table(name="rma_notification")
 * @ORM\HasLifecycleCallbacks
 */
class RmaNotification
{
    /**
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt();
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $rmaNumber;
    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $serialNumber;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $productCode;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $productName;

    /**
     * @ORM\Column(type="date")
     */
    private $purchaseDate;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $purchaseDocument;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $warrantyCard;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $purchasePrice;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $externalComplainCode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\WarrantyType", inversedBy="rmaNotifications")
     * @ORM\JoinColumn(nullable=false)
     */
    private $warrantyType;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $damageDescription;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $configurationSpecification;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="rmaNotifications")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var \DateTime $updatedAt
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RmaStatus", inversedBy="rmaNotification", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $rmaStatus;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getRmaNumber()
    {
        return $this->rmaNumber;
    }

    /**
     * @param mixed $rmaNumber
     */
    public function setRmaNumber($rmaNumber): void
    {
        $this->rmaNumber = $rmaNumber;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getProductCode(): ?string
    {
        return $this->productCode;
    }

    public function setProductCode(string $productCode): self
    {
        $this->productCode = $productCode;

        return $this;
    }

    public function getProductName(): ?string
    {
        return $this->productName;
    }

    public function setProductName(string $productName): self
    {
        $this->productName = $productName;

        return $this;
    }

    public function getPurchaseDate(): ?\DateTimeInterface
    {
        return $this->purchaseDate;
    }

    public function setPurchaseDate(\DateTimeInterface $purchaseDate): self
    {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    public function getPurchaseDocument(): ?string
    {
        return $this->purchaseDocument;
    }

    public function setPurchaseDocument(string $purchaseDocument): self
    {
        $this->purchaseDocument = $purchaseDocument;

        return $this;
    }

    public function getWarrantyCard(): ?string
    {
        return $this->warrantyCard;
    }

    public function setWarrantyCard(string $warrantyCard): self
    {
        $this->warrantyCard = $warrantyCard;

        return $this;
    }

    public function getPurchasePrice(): ?string
    {
        return $this->purchasePrice;
    }

    public function setPurchasePrice(?string $purchasePrice): self
    {
        $this->purchasePrice = $purchasePrice;

        return $this;
    }

    public function getExternalComplainCode(): ?string
    {
        return $this->externalComplainCode;
    }

    public function setExternalComplainCode(?string $externalComplainCode): self
    {
        $this->externalComplainCode = $externalComplainCode;

        return $this;
    }

    public function getWarrantyType(): ?WarrantyType
    {
        return $this->warrantyType;
    }

    public function setWarrantyType(?WarrantyType $warrantyType): self
    {
        $this->warrantyType = $warrantyType;

        return $this;
    }

    public function getDamageDescription(): ?string
    {
        return $this->damageDescription;
    }

    public function setDamageDescription(?string $damageDescription): self
    {
        $this->damageDescription = $damageDescription;

        return $this;
    }

    public function getConfigurationSpecification(): ?string
    {
        return $this->configurationSpecification;
    }

    public function setConfigurationSpecification(?string $configurationSpecification): self
    {
        $this->configurationSpecification = $configurationSpecification;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
        $user->addRmaNotification($this);
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(): void
    {
        $this->updatedAt = new \DateTime("now");
    }

    public function getRmaStatus(): ?RmaStatus
    {
        return $this->rmaStatus;
    }

    public function setRmaStatus(?RmaStatus $rmaStatus): self
    {
        $this->rmaStatus = $rmaStatus;

        return $this;
    }

}
