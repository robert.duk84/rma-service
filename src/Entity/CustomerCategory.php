<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerCategoryRepository")
 */
class CustomerCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $categoryName;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CustomerData", mappedBy="customerCategory")
     */
    private $customerData;

    public function __construct()
    {
        $this->customerData = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->categoryName;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategoryName(): ?string
    {
        return $this->categoryName;
    }

    public function setCategoryName(string $categoryName): self
    {
        $this->categoryName = $categoryName;

        return $this;
    }

    /**
     * @return Collection|CustomerData[]
     */
    public function getCustomerData(): Collection
    {
        return $this->customerData;
    }

    public function addCustomerData(CustomerData $customerData): self
    {
        if (!$this->customerData->contains($customerData)) {
            $this->customerData[] = $customerData;
            $customerData->setCustomerCategory($this);
        }

        return $this;
    }

    public function removeCustomerData(CustomerData $customerData): self
    {
        if ($this->customerData->contains($customerData)) {
            $this->customerData->removeElement($customerData);
            // set the owning side to null (unless already changed)
            if ($customerData->getCustomerCategory() === $this) {
                $customerData->setCustomerCategory(null);
            }
        }

        return $this;
    }
}
