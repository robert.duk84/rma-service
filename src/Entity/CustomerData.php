<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CustomerDataRepository")
 *
 * @ORM\HasLifecycleCallbacks
 */
class CustomerData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     * @ORM\OneToOne(targetEntity="User", inversedBy="customerData", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     *
     * @ORM\Column(type="string",length=10,nullable=false)
     *
     * @Assert\Length(
     *     min=10,
     *     max=10
     * )
     * @Assert\Regex(
     *     pattern="/^\d{10}$/"
     * )
     */
    private $tax_number;

    /**
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $companyName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $streetName;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $houseNumber;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $apartmentNumber;

    /**
     * @ORM\Column(type="string", length=6)
     */
    private $postCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alternativeEmail;

    /**
     * @ORM\Column(type="string",length=12,nullable=false)
     *
     */
    private $mobileNumber;

    /**
     * @ORM\Column(type="string", length=450, nullable=true)
     */
    private $additionalInfo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CustomerCategory", inversedBy="customerData")
     * @ORM\JoinColumn
     */
    private $customerCategory;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getTaxNumber(): ?int
    {
        return $this->tax_number;
    }

    public function setTaxNumber(int $tax_number): self
    {
        $this->tax_number = $tax_number;

        return $this;
    }

    public function getCompanyName(): ?string
    {
        return $this->companyName;
    }

    public function setCompanyName(string $companyName): self
    {
        $this->companyName = $companyName;

        return $this;
    }

    public function getStreetName(): ?string
    {
        return $this->streetName;
    }

    public function setStreetName(string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getHouseNumber(): ?string
    {
        return $this->houseNumber;
    }

    public function setHouseNumber(string $houseNumber): self
    {
        $this->houseNumber = $houseNumber;

        return $this;
    }

    public function getApartmentNumber(): ?string
    {
        return $this->apartmentNumber;
    }

    public function setApartmentNumber(?string $apartmentNumber): self
    {
        $this->apartmentNumber = $apartmentNumber;

        return $this;
    }

    public function getPostCode(): ?string
    {
        return $this->postCode;
    }

    public function setPostCode(string $postCode): self
    {
        $this->postCode = $postCode;

        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city): void
    {
        $this->city = $city;
    }

    public function getAlternativeEmail(): ?string
    {
        return $this->alternativeEmail;
    }

    public function setAlternativeEmail(?string $alternativeEmail): self
    {
        $this->alternativeEmail = $alternativeEmail;

        return $this;
    }

    public function getMobileNumber(): ?int
    {
        return $this->mobileNumber;
    }

    public function setMobileNumber(int $mobileNumber): self
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    public function getAdditionalInfo(): ?string
    {
        return $this->additionalInfo;
    }

    public function setAdditionalInfo(?string $additionalInfo): self
    {
        $this->additionalInfo = $additionalInfo;

        return $this;
    }

    public function isOwner(User $user){
        return $user->getId() === $this->getUser()->getId();
    }

    public function getCustomerCategory(): ?CustomerCategory
    {
        return $this->customerCategory;
    }

    public function setCustomerCategory(?CustomerCategory $customerCategory): self
    {
        $this->customerCategory = $customerCategory;

        return $this;
    }
}
