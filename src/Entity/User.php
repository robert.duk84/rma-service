<?php
declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 * @UniqueEntity(fields={"username"},
 *    message="Adrs {{ username }} już istniej w bazie danej."
 *)
 */
class User extends BaseUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt();
    }

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\UserProfile", mappedBy="user", cascade={"persist", "remove"})
     */
    private $userProfile;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\CustomerData", mappedBy="user", cascade={"persist", "remove"})
     */
    private $customerData;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RmaNotification", mappedBy="user")
     */
    private $rmaNotifications;

    public function __construct()
    {
        parent::__construct();

        $this->setRoles(['ROLE_USER']);
        $this->rmaNotifications = new ArrayCollection();
    }

    /**
     * @param string $email
     *
     * @return BaseUser
     */
    public function setEmail($email)
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    public function getUserProfile(): ?UserProfile
    {
        return $this->userProfile;
    }

    /**
     * @param UserProfile|null $userProfile
     *
     */
    public function setUserProfile(?UserProfile $userProfile)
    {
        $this->userProfile = $userProfile;
            if ($userProfile)
                $userProfile->setUser($this);

    }

    /**
     * @return CustomerData|null
     */
    public function getCustomerData(): ?CustomerData
    {
        return $this->customerData;
    }
    /**
     * @param CustomerData|null $customerData
     */
    public function setCustomerData(?CustomerData $customerData)
    {
        $this->customerData = $customerData;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(): void
    {
        $this->updatedAt = new \DateTime('now');
    }

    /**
     * @return Collection|RmaNotification[]
     */
    public function getRmaNotifications(): Collection
    {
        return $this->rmaNotifications;
    }

    public function addRmaNotification(RmaNotification $rmaNotification): void
    {
        if (!$this->rmaNotifications->contains($rmaNotification)) {
            $this->rmaNotifications[] = $rmaNotification;
            $rmaNotification->setUser($this);
        }

//        return $this;
//        $this->rmaNotifications[] = $rmaNotification;
    }

    public function removeRmaNotification(RmaNotification $rmaNotification): self
    {
        if ($this->rmaNotifications->contains($rmaNotification)) {
            $this->rmaNotifications->removeElement($rmaNotification);
            // set the owning side to null (unless already changed)
            if ($rmaNotification->getUser() === $this) {
                $rmaNotification->setUser(null);
            }
        }

        return $this;
    }
}
