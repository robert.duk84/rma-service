<?php

namespace App\Controller;

use App\Entity\CustomerData;
use App\Form\CustomerDataForm;
use App\Form\CustomerDataSetupFormType;
use App\Repository\CustomerDataRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;


class CustomerDataController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/customer/registration", name="customer_data_registration")
     * @param Request $request
     * @param CustomerDataRepository $customerDataRepository
     * @return Response
     */
    public function customerDataRegistration (Request $request, CustomerDataRepository $customerDataRepository)
    {
        $customer = new CustomerData;
        $customer->setUser($this->getUser());

        if($customer->getUser()->getCustomerData() !== null)
        {
            $customer = $customerDataRepository->find($this->getUser()->getCustomerData()->getId());
            $form = $this->createForm(CustomerDataForm::class, $customer);
        } else {
            $form = $this->createForm(CustomerDataForm::class, $customer);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $customer = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($customer);
            $em->flush();
            $this->addFlash('success', 'Dane zostały zapisane poprawnie. Dodaj swoją reklamacje.');

            return $this->redirectToRoute('panel_index');
        }

        return $this->render('customer_data/customer_registration.html.twig', [
            'form' => $form->createView(),
            'userCompany' => $this->getUser()->getUserProfile()->getCompanyName(),
        ]);
    }

    /**
     * @Route("/customer/data-setup", name="customer_data_setup")
     * @param Request $request
     * @return Response
     */
    public function customerDataUpdate(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $form = $this->createForm(CustomerDataSetupFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($form->getData());
            $this->entityManager->flush();
            $this->addFlash('success', 'Informacje zostały zaktualizowane');

            return $this->redirectToRoute('panel_index');
        }
        return $this->render('panel/customer_data_setup.html.twig', [
            'form' => $form->createView(),
            'user' => $user
        ]);
    }


}
