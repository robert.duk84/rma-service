<?php

namespace App\Controller;

use App\Repository\RmaNotificationRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var RmaNotificationRepository
     */
    private $rmaNotificationRepository;

    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager, RmaNotificationRepository $rmaNotificationRepository)
    {
        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
        $this->rmaNotificationRepository = $rmaNotificationRepository;
    }

    /**
     * @Route("/", name="home_index")
     */
    public function index()
    {
        return $this->render('home/home.html.twig');
    }

    /**
     * @Route("/panel", name="panel_index")
     *
     * @return Response
     */
    public function panel(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $rmaNotifications = $this->getUser()->getRmaNotifications();
        
        return $this->render('panel/index.html.twig', [
            'user' => $this->getUser(),
            'rma_notification' => $rmaNotifications
        ]);
    }

    /**
     * @Route("/customer_reports", name="customer_reports")
     */
    public function customerReports(): Response
    {
        return $this->render('panel/customer_reports.html.twig');
    }

    /**
     * @Route("/panel_admin", name="panel_admin")
     */
    public function panel_admin(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN');

        return $this->render('panel_admin/index.html.twig');
    }

    /**
     * @Route("/customer_finances", name="customer_finances")
     */
    public function customerFinances(): Response
    {
        return $this->render('panel/customer_finances.html.twig');
    }

    /**
     * @Route("/panel_faq", name="panel_faq")
     */
    public function panelFaq(): Response
    {
        return $this->render('panel/panel_faq.html.twig');
    }

    /**
     * @Route("/pricing_plan", name="pricing_plan")
     */
    public function pricingPlan(): Response
    {
        return $this->render('panel/pricing_plan.html.twig');
    }
}
