<?php

namespace App\Controller;

use App\Entity\RmaNotification;
use App\Entity\RmaStatus;
use App\Form\RmaNotificationType;
use App\Repository\RmaStatusRepository;
use App\Service\Pagination\Pagination;
use App\Service\RmaUniqueCodeGenerator\RMAUniqueCodeGenerator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rma/notification")
 */
class RmaNotificationController extends AbstractController
{

    /**
     * @var RmaStatusRepository
     */
    private $statusRepository;

    public function __construct(RmaStatusRepository $statusRepository)
    {
        $this->statusRepository = $statusRepository;
    }

    /**
     * @Route("/", name="rma_notification_index", methods={"GET"})
     * @param Request                   $request
     * @param PaginatorInterface        $paginator
     * @param Pagination                $pagination
     *
     * @return Response
     */
    public function index(
        Request $request,
        PaginatorInterface $paginator,
        Pagination $pagination
    ): Response {
        $rmaNotifications = $this->getUser()->getRmaNotifications();

        $pageLimitItems = $pagination->getPagination($request, $paginator, $rmaNotifications);

        return $this->render('rma_notification/index.html.twig', [
            'rma_notifications' => $pageLimitItems,
        ]);
    }

    /**
     * @Route("/new", name="rma_notification_new", methods={"GET","POST"})
     * @param Request                $request
     * @param RMAUniqueCodeGenerator $RMAcodeGenerator
     * @return Response
     */
    public function new(Request $request, RMAUniqueCodeGenerator $RMAcodeGenerator): Response
    {
        $rmaNotification = new RmaNotification();
        $rmaNotification->setUser($this->getUser());
        $rmaNotification->setRmaNumber($RMAcodeGenerator->generate());

        $rmaStatus = new RmaStatus();
        $rmaStatus->setName($this->statusRepository->findOneBy(['id' => 1]));
        $rmaNotification->setRmaStatus($rmaStatus);

        $form = $this->createForm(RmaNotificationType::class, $rmaNotification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $rmaNotification = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($rmaNotification);
            $entityManager->flush();
            $this->addFlash('success', 'Reklamacja została poprawnie dodana');
            return $this->redirectToRoute('rma_notification_edit', ['id' => $rmaNotification->getId()]);
        }

        return $this->render('rma_notification/new.html.twig', [
            'rma_notification' => $rmaNotification,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="rma_notification_show", methods={"GET"})
     * @param RmaNotification $rmaNotification
     *
     * @return Response
     */
    public function show(RmaNotification $rmaNotification): Response
    {
        return $this->render('rma_notification/show.html.twig', [
            'user' => $this->getUser(),
            'customerData' => $rmaNotification->getUser()->getCustomerData(),
            'rma_notification' => $rmaNotification,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="rma_notification_edit", methods={"GET","POST"})
     * @param Request         $request
     * @param RmaNotification $rmaNotification
     *
     * @return Response
     */
    public function edit(Request $request, RmaNotification $rmaNotification): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(RmaNotificationType::class, $rmaNotification);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Reklamacja została zaaktualizowana');
            return $this->redirectToRoute('rma_notification_index', [
                'id' => $rmaNotification->getId(),
            ]);
        }

        return $this->render('rma_notification/edit.html.twig', [
            'rma_notification' => $rmaNotification,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="rma_notification_delete", methods={"DELETE"})
     * @param Request         $request
     * @param RmaNotification $rmaNotification
     *
     * @return Response
     */
    public function delete(Request $request, RmaNotification $rmaNotification): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        if ($this->isCsrfTokenValid('delete'.$rmaNotification->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($rmaNotification);
            $entityManager->flush();
        }

        return $this->redirectToRoute('rma_notification_index');
    }
}
