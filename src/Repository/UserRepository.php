<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\Expr;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    #// TODO: !!! filtracj dla panelu admin do dodania route na super admina
//    /**
//     * @Route("/admin", name="admin")
//     */
//    public function index()
//    {
//        return $this->render('admin/users.html.twig', [
//            'users' => $this->userRepository->getAllUsersForAdmin()
//        ]);
//    }
//
    public function getAllUsersInfo(){
        return $this->createQueryBuilder('u')
            ->where('u.roles NOT LIKE : role')
            ->setParameter('role', '%ROLE_SUPER_ADMIN')
            ->getQuery()
            ->getResult();
    }

    public function getUserFullRegisteredUser(){
        $qb = $this->createQueryBuilder('u')
            ->join('u.customerData', 'customerData')
            ->andWhere(new Expr\Comparison('customerData', Expr\Comparison::NEQ, 0))
            ->andWhere(new Expr\Comparison('u.enabled', Expr\Comparison::NEQ, 0))
            ->orderBy('u.id', 'DESC')
            ->getQuery()
            ->getResult();
        return $qb;
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
