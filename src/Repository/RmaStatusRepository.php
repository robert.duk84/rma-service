<?php

namespace App\Repository;

use App\Entity\RmaStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RmaStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method RmaStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method RmaStatus[]    findAll()
 * @method RmaStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RmaStatusRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RmaStatus::class);
    }
}
