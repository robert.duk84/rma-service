<?php

namespace App\Repository;

use App\Entity\RmaNotification;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RmaNotification|null find($id, $lockMode = null, $lockVersion = null)
 * @method RmaNotification|null findOneBy(array $criteria, array $orderBy = null)
 * @method RmaNotification[]    findAll()
 * @method RmaNotification[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RmaNotificationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RmaNotification::class);
    }

    public function findByRMACode($RMAcode)
    {
        return $this->createQueryBuilder('rma')
            ->andWhere('rma.rmaNumber = :RMAcode')
            ->setParameter('RMAcode', $RMAcode)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return RmaNotification[] Returns an array of RmaNotification objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RmaNotification
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
