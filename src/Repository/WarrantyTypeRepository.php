<?php

namespace App\Repository;

use App\Entity\WarrantyType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method WarrantyType|null find($id, $lockMode = null, $lockVersion = null)
 * @method WarrantyType|null findOneBy(array $criteria, array $orderBy = null)
 * @method WarrantyType[]    findAll()
 * @method WarrantyType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WarrantyTypeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, WarrantyType::class);
    }

    // /**
    //  * @return WarrantyType[] Returns an array of WarrantyType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?WarrantyType
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
