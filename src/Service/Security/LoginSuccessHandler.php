<?php

namespace App\Service\Security;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface{

    /**
     * @var Security
     */
    private $security;
    /**
     * @var RouterInterface
     */
    private $router;

    public function __construct(Security $security, RouterInterface $router) {
        $this->security = $security;
        $this->router = $router;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        if($this->security->isGranted('ROLE_SUPER_ADMIN')){
            return new RedirectResponse($this->router->generate('panel_admin'));
        }
        return new RedirectResponse($this->router->generate('panel_index'));
    }
}