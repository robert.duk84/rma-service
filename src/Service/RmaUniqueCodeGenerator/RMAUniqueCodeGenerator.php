<?php
declare(strict_types=1);

namespace App\Service\RmaUniqueCodeGenerator;

use App\Repository\RmaNotificationRepository;
use Doctrine\ORM\EntityManagerInterface;

class RMAUniqueCodeGenerator
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var RmaNotificationRepository
     */
    private $rmaNotificationRepository;

    public function __construct(EntityManagerInterface $entityManager, RmaNotificationRepository $rmaNotificationRepository)
    {
        $this->entityManager = $entityManager;
        $this->rmaNotificationRepository = $rmaNotificationRepository;
    }

    const NUMBER_OF_DIGITS = 5;

    private $symbols = '0123456789';

    public function generate(): string
    {
        while (true) {
            $RMAcode = $this->getRandomCode();

            if ($this->rmaNotificationRepository->findByRMACode($RMAcode) === null) {
                return $RMAcode;
            }
        }
    }

    public function getRandomCode(){
        $randomCode = '';
        $countOfCharts = strlen($this->symbols);
        for ($i = 0; $i < self::NUMBER_OF_DIGITS; $i++) {
            $randomCode .= $this->symbols[rand(0, $countOfCharts - 1)];
        }
        $RMAcode = 'RMA'.$randomCode;

        return $RMAcode;
    }
}
