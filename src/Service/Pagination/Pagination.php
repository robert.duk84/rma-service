<?php

namespace App\Service\Pagination;

class Pagination
{
    public function getPagination($request, $paginator, $data)
    {
        $paginatedItems = $paginator->paginate(
            $data, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        $paginatedItems->setCustomParameters([
            'align' => 'right', # center|right (for template: twitter_bootstrap_v4_pagination)
        ]);

        return $paginatedItems;
    }
}